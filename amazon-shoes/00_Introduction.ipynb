{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e3ccfb14",
   "metadata": {},
   "source": [
    "# Welcome! \n",
    "\n",
    "In this self-paced workshop, you'll walk through a complete end-to-end example of using Hugging Face Transformers, involving both our open-source libraries and our commercial products. \n",
    "\n",
    "Starting from a dataset containing real-life product reviews from Amazon.com, you're going to train and deploy a text classification model predicting the star rating for similar reviews. All code will be available to you, and you'll easily be able to use it in your own projects.\n",
    "\n",
    "Along the way, this is what you're going to learn:\n",
    "\n",
    "* Working with datasets\n",
    "\n",
    "   * Browse datasets on the [Hugging Face Hub](https://huggingface.co),\n",
    "\n",
    "   * Download and process a dataset with the [datasets](https://github.com/huggingface/datasets) library,\n",
    "\n",
    "   * Save a processed dataset to disk,\n",
    "\n",
    "   * Create a dataset repository with the [Hugging Face CLI](https://github.com/huggingface/huggingface_hub),\n",
    "   \n",
    "   * Write a dataset card.\n",
    "\n",
    "\n",
    "* Working with models, part 1: the basics\n",
    "\n",
    "   * Browse models on the [Hugging Face Hub](https://huggingface.co),\n",
    "   \n",
    "   * Test models with the inference widget,\n",
    "\n",
    "   * Fine-tune a Hugging Face Transformer with the Trainer API from the [transformers](https://github.com/huggingface/transformers) library,\n",
    "\n",
    "   * Create a model repository with the [Hugging Face CLI](https://github.com/huggingface/huggingface_hub),\n",
    "\n",
    "   * Write a model card,\n",
    "   \n",
    "   * Load a model with the Pipeline API, and predict with it,\n",
    "\n",
    "   * Deploy a model on the [Hugging Face Inference API](https://huggingface.co/inference-api), and predict with it.\n",
    "   \n",
    "\n",
    "* Working with models, part 2: training and deploying at scale on [Amazon SageMaker](https://aws.amazon.com/sagemaker)\n",
    "\n",
    "   * Adapt your training script to run on SageMaker,\n",
    "   \n",
    "   * Use the SageMaker SDK to launch a training job,\n",
    "   \n",
    "   * Use the SageMaker SDK to deploy a model on a SageMaker Endpoint,\n",
    "   \n",
    "   * Use the SageMaker SDK to predict with an endpoint.\n",
    "\n",
    "\n",
    "* Using AutoML to train models\n",
    "\n",
    "   * Launch an AutoML job with [AutoTrain](https://huggingface.co/autotrain),\n",
    "\n",
    "   * Work with AutoTrain datasets and models.\n",
    "\n",
    "   \n",
    "* Showcasing your models with Spaces\n",
    "\n",
    "   * Write a simple Gradio application to showcase your model,\n",
    "   \n",
    "   * Deploy the application to [Hugging Face Spaces](https://huggingface.co/spaces), and test it.\n",
    "   \n",
    "   \n",
    "* Accelerating models with Machine Learning hardware\n",
    "\n",
    "   * Predicting with [Hugging Face Optimum](https://huggingface.co/optimum) and ONNX optimization\n",
    "   \n",
    "      * Export a model to ONNX format,\n",
    "   \n",
    "      * Optimize an ONNX model with ,\n",
    "   \n",
    "      * Quantize an ONNX model with Optimum.\n",
    "\n",
    "   * Training with [Hugging Face Optimum](https://huggingface.co/optimum) and [Habana Gaudi](https://aws.amazon.com/ec2/instance-types/dl1/) accelerators on Amazon EC2\n",
    "\n",
    "      * Launch an Habana Gaudi instance on [Amazon EC2](https://aws.amazon.com/ec2/instance-types/dl1/),\n",
    "   \n",
    "      * Adapt your training script to run on Habana Gaudi,\n",
    "   \n",
    "      * Train on a single Habana Processor Unit (HPU),\n",
    "   \n",
    "      * Run distributed training on 8 HPUs.\n",
    "\n",
    "\n",
    "   \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f78748c",
   "metadata": {},
   "source": [
    "Let's started. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f78bb94f",
   "metadata": {},
   "source": [
    "## Defining the business problem\n",
    "\n",
    "The first step in any ML project is to clearly state the **business** problem we're trying to solve. \n",
    "\n",
    "Here, let's assume that we work for a shoe retailer. We sell tens of thousands of different products, and we'd like to understand the voice of our customers by collecting reviews posted anywhere on the Internet (customer support emails, social networks, consumer forums, etc.), and scoring them.\n",
    "\n",
    "There are many ways to score these reviews: sentiment analysis, emotion detection, numerical score, etc. After some initial discussion with business stakeholders, we decide to go with the well-know star rating available on Amazon.com."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5351c68a",
   "metadata": {},
   "source": [
    "![Sample reviews from Amazon.com with star ratings](images/01.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce6ef347",
   "metadata": {},
   "source": [
    "Now, we can define our business problem as: \"*Given a English language product review for shoes, I want to predict a star rating between 1 and 5 to understand how satisfied the customer is with their purchase.*\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af14c500",
   "metadata": {},
   "source": [
    "## Framing the business problem as a Machine Learning problem\n",
    "\n",
    "Translating a business problem to a Machine Learning problem can be quite complicated. Here, it's pretty straighforward: we need to train a **multi-class classification model** for product reviews. The model should be able to accurately predict the probabilities for each one of the five star ratings.\n",
    "\n",
    "We need to define a metric that will tell us how well the model is performing. For multi-class classification, we can start with well-known metrics such as [accuracy](https://en.wikipedia.org/wiki/Accuracy_and_precision) and the [F1 score](https://en.wikipedia.org/wiki/F-score).\n",
    "\n",
    "Thus, the first definition of our Machine Learning problem is: \"*Given a test set for English language shoe product reviews, I want to classify each review according to star ratings between 1 and 5, with an accuracy of at least xx% and an F1 score of at least 0.xy.\"\n",
    "\n",
    "If you're able to put numbers on accuracy and the F1 score at this point, congratulations, because I can't! Maybe your stakeholders or your company rules say that accuracy should be at least 80%, but at this point, it's a wild guess. We need to experiment first and define a baseline before we can actually set a reasonable target."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad3662c5",
   "metadata": {},
   "source": [
    "## Looking for models\n",
    "\n",
    "One of the key benefits of Transformer models is how good they are at Transfer Learning. Thus, our next step should be to head out to the [Hugging Face Hub](https://huggingface.co), and explore English language [text classification](https://huggingface.co/models?language=en&pipeline_tag=text-classification&sort=downloads) models. \n",
    "\n",
    "There are several [sentiment analysis](https://huggingface.co/models?language=en&pipeline_tag=text-classification&sort=downloads&search=sentiment) models which we can quickly try. However, it looks like we can't find a model for shoe reviews and star ratings.\n",
    "\n",
    "No problem. We can start from one of the many [language models](https://huggingface.co/models?language=en&pipeline_tag=fill-mask&sort=downloads) trained on English language corpuses, and fine-tune them on a text classification task."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05a48c36",
   "metadata": {},
   "source": [
    "![Models](images/02.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67a196f5",
   "metadata": {},
   "source": [
    "## Looking for datasets"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02f14331",
   "metadata": {},
   "source": [
    "Speaking of fine-tuning, are there any [datasets](https://huggingface.co/datasets) that we could use? As a matter of fact, yes! There are quite a few [product reviews](https://huggingface.co/datasets?languages=languages:en&sort=downloads&search=reviews) datasets in English (feel free to look at other languages too!)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8fad5ad7",
   "metadata": {},
   "source": [
    "![Product reviews datasets](images/03.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c210006",
   "metadata": {},
   "source": [
    "The [Amazon US reviews](https://huggingface.co/datasets/amazon_us_reviews) dataset is very interesting. It even has a subset for shoe reviews, which we can explore with the [dataset viewer](https://huggingface.co/datasets/amazon_us_reviews/viewer/Shoes_v1_00/train)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adcbe557",
   "metadata": {},
   "source": [
    "![Shoe reviews](images/04.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce9a7fe5",
   "metadata": {},
   "source": [
    "Language models and a shoe review dataset are enough to get started. In the next notebook, we're going to load our dataset, explore it a bit and process it for training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e5152138",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
